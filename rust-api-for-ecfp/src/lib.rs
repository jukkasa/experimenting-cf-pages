use argon2::{
    password_hash::{rand_core::OsRng, PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Argon2,
};
use base64ct::{Base64, Encoding};
use cookie::{time::Duration, Cookie, CookieJar, Key, SameSite};
use once_cell::sync::OnceCell;
use serde::Deserialize;
use serde_json::json;
use worker::*;

mod db;
mod user;
mod utils;

use crate::user::User;

// Global environmental/secret values
static SESSION_KEY: OnceCell<[u8; 64]> = OnceCell::new();
pub static UPSTASH_URL: OnceCell<String> = OnceCell::new();
pub static UPSTASH_TOKEN: OnceCell<String> = OnceCell::new();

// For both REST API and database value

fn log_request(req: &Request) {
    console_log!(
        "{} - [{}], located at: {:?}, within: {}",
        Date::now().to_string(),
        req.path(),
        req.cf().coordinates().unwrap_or_default(),
        req.cf().region().unwrap_or_else(|| "unknown region".into())
    );
}

#[event(fetch)]
pub async fn main(req: Request, env: Env, _ctx: worker::Context) -> Result<Response> {
    log_request(&req);
    utils::set_panic_hook();

    // Get cookie session secret key (must be 64 bytes encoded with base64)
    let session_key = env.secret("SESSION_KEY")?.to_string();

    // Set global constants (once_cell)
    let mut key: [u8; 64] = [0; 64];
    let _ = Base64::decode(session_key, &mut key).unwrap();
    let _ = SESSION_KEY.set(key);

    let _ = UPSTASH_URL.set(env.secret("UPSTASH_URL")?.to_string());
    let _ = UPSTASH_TOKEN.set(env.secret("UPSTASH_TOKEN")?.to_string());

    // API routes
    let router = Router::new();
    router
        .get("/", |_, _| Response::ok("Hello from Workers!"))
        .get("/me", me)
        .post_async("/login", login)
        .post_async("/register", register)
        .post("/logout", logout)
        .post_async("/chpassword", change_password)
        .options("/me", options)
        .options("/login", options)
        .options("/register", options)
        .options("/chpassword", options)
        .run(req, env)
        .await
}

// Create suitable cors headers for API
fn create_cors(req: &Request) -> Cors {
    let origin = req
        .headers()
        .get("Origin")
        .unwrap()
        .unwrap_or_else(|| "*".to_string());
    // TODO: check if startswith "http://0.0.0.0", "http://127", "http://localhost", "https://experimenting"
    Cors::default()
        .with_origins([origin])
        .with_credentials(true)
        .with_allowed_headers(["Content-Type"])
}

// Error response (JSON)
fn error_response(req: &Request, status_code: u16, message: &str) -> Result<Response> {
    let cors = create_cors(req);
    let response = Response::from_json(&json!({ "error": message }))?
        .with_status(status_code)
        .with_cors(&cors)?;
    Ok(response)
}

// User data response
fn user_response(req: &Request, user: &User) -> Result<Response> {
    let cors = create_cors(req);
    let resp_user = User {
        password: None,
        ..user.clone()
    };
    let mut response = Response::from_json(&resp_user)?.with_cors(&cors)?;
    let cookie_string = create_session_cookie_string(&resp_user);
    let _ = response.headers_mut().set("Set-Cookie", &cookie_string)?;
    Ok(response)
}

// Decode user data from session cookie
fn session_user(req: &Request) -> Option<User> {
    let cookie_str = req.headers().get("Cookie").unwrap();
    cookie_str.as_ref()?;
    let cookie_str = cookie_str.unwrap();

    let key = Key::from(SESSION_KEY.get().unwrap());
    let mut jar = CookieJar::new();
    if let Ok(cookie) = Cookie::parse(cookie_str) {
        jar.add(cookie);
        if let Some(decoded_cookie) = jar.private(&key).get("SESSION") {
            let user_json = &decoded_cookie.to_string()[8..];
            return match serde_json::from_str::<User>(user_json) {
                Ok(user) => Some(user),
                Err(err) => {
                    console_log!("Error parsing user from cookie: {}", err);
                    None
                }
            };
        }
    }

    None
}

fn create_session_cookie_string(user: &User) -> String {
    let user_string = serde_json::to_string(user).unwrap_or_default();
    let cookie = Cookie::build("SESSION", user_string)
        .path("/")
        .same_site(SameSite::None)
        .secure(true)
        .http_only(true)
        .max_age(Duration::days(1))
        .finish();

    let key = Key::from(SESSION_KEY.get().unwrap());
    let mut jar = CookieJar::new();
    jar.private_mut(&key).add(cookie);
    let cookie = jar.get("SESSION").unwrap();
    cookie.to_string()
}

/****** API route handler functions *****/

// CORS query from UI client
fn options(req: Request, _ctx: RouteContext<()>) -> Result<Response> {
    let cors = create_cors(&req);
    Response::empty()?.with_cors(&cors)
}

#[derive(Deserialize, Debug)]
struct DbResponse {
    result: Option<String>,
}

#[derive(Deserialize, Debug)]
struct LoginReq {
    username: String,
    password: String,
}

// API: /me handler
fn me(req: Request, _ctx: RouteContext<()>) -> Result<Response> {
    let user = session_user(&req);

    let cors = create_cors(&req);
    match user {
        Some(user) => Response::from_json(&user)?.with_cors(&cors),
        None => Response::error("Not logged in", 401)?.with_cors(&cors),
    }
}

// API: /login handler
async fn login(mut req: Request, ctx: RouteContext<()>) -> Result<Response> {
    let login_data = match req.json::<LoginReq>().await {
        Ok(data) => data,
        Err(err) => {
            return error_response(&req, 401, &format!("Invalid login: {}", err));
        }
    };

    let db_user_data = db::db_get_user(&login_data.username, &ctx).await?;
    let db_resp = serde_json::from_str::<DbResponse>(&db_user_data);
    match db_resp {
        Ok(result) => match result.result {
            Some(json) => {
                let mut user: User = serde_json::from_str(&json).unwrap_or_default();
                let db_pass = user.password.unwrap_or_default();
                let parsed_hash = PasswordHash::new(&db_pass);
                match parsed_hash {
                    Ok(parsed_hash) => {
                        if Argon2::default()
                            .verify_password(login_data.password.as_bytes(), &parsed_hash)
                            .is_ok()
                        {
                            user.password = None;
                            user_response(&req, &user)
                        } else {
                            error_response(&req, 401, "FIXME: incorrect password")
                        }
                    }
                    Err(err) => {
                        return error_response(&req, 500, &format!("DB corrupt?: {}", err));
                    }
                }
            }
            None => {
                return error_response(
                    &req,
                    401,
                    &format!("FIXME: no such user: {}", login_data.username),
                );
            }
        },
        Err(err) => {
            return error_response(&req, 500, &format!("Unexpected data from db: {}", err));
        }
    }
}

// API: /register handler
async fn register(mut req: Request, ctx: RouteContext<()>) -> Result<Response> {
    let register_data: LoginReq;

    match req.json::<LoginReq>().await {
        Ok(data) => {
            register_data = data;
            // Validate
            if register_data.username.is_empty() || register_data.password.is_empty() {
                return error_response(&req, 400, "Failed validation");
            }
        }
        Err(err) => {
            return error_response(&req, 400, &format!("Invalid register request: {}", err));
        }
    }

    let old_user_data = db::db_get_user(&register_data.username, &ctx).await?;
    let old_user: serde_json::Value = serde_json::from_str(&old_user_data).unwrap_or_default();
    if !old_user["result"].is_null() {
        return error_response(
            &req,
            401,
            &format!("User {} already exists", register_data.username),
        );
    }
    // Hash password
    let salt = SaltString::generate(&mut OsRng);
    let argon2 = Argon2::default(); // Argon2 with default params (Argon2id v19)

    // Hash password to PHC string ($argon2id$v=19$...)
    let password_hash = argon2.hash_password(register_data.password.as_bytes(), &salt);
    if password_hash.is_err() {
        return error_response(
            &req,
            500,
            &format!("password hashing error: {:?}", password_hash),
        );
    };
    let password_hash = password_hash.unwrap().to_string();

    // Create new User, serialize it and write to DB
    let new_user = User {
        username: register_data.username,
        password: Some(password_hash),
        ..Default::default()
    };
    let user_data = serde_json::to_string(&new_user).unwrap_or_default(); // TODO: error handling
    let _db_result = db::db_set_user(&new_user.username, &user_data, &ctx).await?;

    // Respond to user with user data
    user_response(&req, &new_user)
}

// API: /logout handler
fn logout(req: Request, _ctx: RouteContext<()>) -> Result<Response> {
    let cors = create_cors(&req);
    let mut response = Response::ok("Logging out")?.with_cors(&cors)?;
    let _ = response.headers_mut().set(
        "Set-Cookie",
        "SESSION=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT",
    )?;

    Ok(response)
}

// Not implemented yet
async fn change_password(req: Request, _ctx: RouteContext<()>) -> Result<Response> {
    error_response(
        &req,
        404,
        &format!("API at {:?} not implemented", req.url()),
    )
}
