/****** Database getters and setters *******/
// Upstash Redis REST API

use worker::*;

pub async fn db_get_user(username: &str, _ctx: &RouteContext<()>) -> Result<String> {
    // let upstash_url = ctx.secret("UPSTASH_URL")?.to_string();
    // let upstash_token = ctx.secret("UPSTASH_TOKEN")?.to_string();
    let upstash_url = crate::UPSTASH_URL.get().unwrap();
    let upstash_token = crate::UPSTASH_TOKEN.get().unwrap();

    let mut request = Request::new(
        format!("{}/get/user:{}", upstash_url, username).as_str(),
        Method::Get,
    )?;
    request.headers_mut()?.set(
        "Authorization",
        format!("Bearer {}", upstash_token).as_str(),
    )?;

    let fetch = Fetch::Request(request);
    let mut resp = fetch.send().await?;

    if resp.status_code() != 200 {
        console_log!(
            "Upstash get user returned error status {}",
            resp.status_code()
        );
        return Ok(String::new());
    }

    let t = resp.text().await?;
    Ok(t)
}

pub async fn db_set_user(
    username: &str,
    state: &str,
    _ctx: &RouteContext<()>,
) -> Result<Option<String>> {
    let upstash_url = crate::UPSTASH_URL.get().unwrap();
    let upstash_token = crate::UPSTASH_TOKEN.get().unwrap();

    let mut init = RequestInit::new();
    let init = init.with_method(Method::Post).with_body(Some(state.into()));
    let uri = format!("{}/set/user:{}", upstash_url, username);
    let mut request = Request::new_with_init(uri.as_str(), init)?;
    request.headers_mut()?.set(
        "Authorization",
        format!("Bearer {}", upstash_token).as_str(),
    )?;

    let fetch = Fetch::Request(request);
    let mut resp = fetch.send().await?;

    if resp.status_code() != 200 {
        console_log!(
            "Upstash get user returned error status {}",
            resp.status_code()
        );
        return Ok(None);
    }

    let t = resp.text().await?;
    Ok(Some(t))
}
