# experimenting-cf-pages

Just dummy test of Cloudflare Pages + API with Cloudflare Workers (in Rust).
User registration and login/logout.

## Installation and configuration notes

### Frontend (`public`)

In Cloudflare Pages project settings:
- set environment variable `API_URL` (to e.g. https://api-for-ecfp.yourworkerssubdomain.workers.dev)
- In build configuration, set build command as `./build.sh` and build output directory `/public`

### Backend API (`rust-api-for-ecfp`)

- Written with https://github.com/cloudflare/workers-rs
- Uses https://www.upstash.com/redis as database (with REST API)
- Set Workers secrets:
   - UPSTASH_URL (e.g. https://eu1-something-12345.upstash.io)
   - UPSTASH_TOKEN
   - SESSION_KEY (64 random bytes, base64-encoded. Generate with e.g. `openssl rand -base64 64`)